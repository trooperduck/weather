<?php


namespace Drupal\Tests\weather\Unit;


use Drupal\weather\Client\OpenWeatherCurrentClient;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;

trait OpenWeatherClientTrait {

  /**
   * Guzzle MockHandler to simulate responses.
   *
   * @var \GuzzleHttp\Handler\MockHandler $mock
   */
  protected $mock;

  /**
   * @var OpenWeatherCurrentClient
   */
  protected $open_weather_current_client;

  /**
   * Test setup.
   */
  protected function setUp() {
    $this->mock = new MockHandler();

    $http_client = new Client([
      'handler' => $this->mock,
    ]);

    $config_factory = $this->getConfigFactoryStub([
      'weather.weatherconfig' => ['app_id_key' => 'b6907d289e10d714a6e88b30761fae22'],
    ]);

    $this->open_weather_current_client = new OpenWeatherCurrentClient($http_client, $config_factory);
  }

}
