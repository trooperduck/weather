<?php


namespace Drupal\Tests\weather\Unit;

use GuzzleHttp\Psr7\Response;
use Drupal\Tests\UnitTestCase;
use Drupal\weather\Client\OpenWeatherCurrentClient;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;

/**
 * Tests OpenWeatherCurrentClient
 *
 * @group weather
 */
class OpenWeatherCurrentClientTest extends UnitTestCase {

  use OpenWeatherClientTrait;

  /**
   * Test successful and unauthorized response for a current weather by city
   * request.
   */
  public function testOpenWeatherCurrentClient() {
    // Successful response.
    $this->mock->append(
      new Response(200, [], file_get_contents(__DIR__ . '/../../assets/openweather.current.response.json'))
    );
    $data = $this->open_weather_current_client->getCurrentWeatherByCity('Helmond,nl');

    $this->assertEquals('Helmond', $data['name']);

    // Unauthorized response.
    $this->mock->append(
      new Response(401, [], file_get_contents(__DIR__ . '/../../assets/openweather.current.unauthorized.response.json'))
    );
    $data = $this->open_weather_current_client->getCurrentWeatherByCity('Helmond,nl');

    $this->assertEquals('Invalid API key. Please see http://openweathermap.org/faq#error401 for more info.', $data['message']);
  }
}
