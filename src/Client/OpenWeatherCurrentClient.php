<?php

namespace Drupal\weather\Client;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class OpenWeatherCurrentClient.
 */
class OpenWeatherCurrentClient implements OpenWeatherClientInterface {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new OpenWeatherCurrentClient object.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
  }

  /**
   * Send request to OpenWeather.
   *
   * @param array $extraParams
   *
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function request(array $extraParams = []) {
    $config = $this->configFactory->get('weather.weatherconfig');
    $app_id = $config->get('app_id_key');
    $params = array_merge([
      'appid' => $app_id,
    ], $extraParams);
    $uri = 'api.openweathermap.org/data/2.5/weather';

    $response = $this->httpClient->request('get', $uri, ['query' => $params]);

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * @param string $city
   *
   * @return \Psr\Http\Message\ResponseInterface
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getCurrentWeatherByCity(string $city) {
    $extraParams = ['q' => $city];
    return $this->request($extraParams);
  }
}
