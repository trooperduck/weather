<?php

namespace Drupal\weather\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class WeatherConfigForm.
 */
class WeatherConfigForm extends ConfigFormBase {


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'weather.weatherconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weather_config_form';
  }

  /**
   * Location of your api keys in your profile on OpenWeather.
   */
  const API_URL = 'https://home.openweathermap.org/api_keys';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $url = Url::fromUri(static::API_URL);
    $link = new Link($url->toUriString(), $url);

    $config = $this->config('weather.weatherconfig');
    $form['openweather_key'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('OpenWeather key'),
    ];
    $form['openweather_key']['app_id_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID key'),
      '#description' => $this->t('Enter the App ID provided to you after registering with OpenWeather (@link).', [
        '@link' => $link->toString(),
      ]),
      '#maxlength' => 32,
      '#size' => 32,
      '#default_value' => $config->get('app_id_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('weather.weatherconfig')
      ->set('app_id_key', $form_state->getValue('app_id_key'))
      ->save();
  }

}
