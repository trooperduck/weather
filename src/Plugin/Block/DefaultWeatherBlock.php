<?php

namespace Drupal\weather\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\weather\Client\OpenWeatherCurrentClient;
use PhpUnitConversion\Unit\Temperature;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DefaultWeatherBlock' block.
 *
 * @Block(
 *  id = "default_weather_block",
 *  admin_label = @Translation("Weather"),
 * )
 */
class DefaultWeatherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var OpenWeatherCurrentClient
   */
  protected $client;

  /**
   * OpenWeather service.
   *
   * @var $openWeather
   */
  //  protected $temperatureConverter;

  /**
   * Constructs a new DefaultWeatherBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param OpenWeatherCurrentClient $client
   *   Injected http client service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    OpenWeatherCurrentClient $client
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('weather.openweather.current.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#description' => $this->t('Choose the city for which you want to retrieve the weather data. Additionally you can provide a country code to be less ambiguous in the form of: "City name, country code"'),
      '#default_value' => $this->configuration['city'] ?? 'London',
      '#maxlength' => 64,
      '#length' => 64,
    ];
    $form['units'] = [
      '#type' => 'radios',
      '#title' => $this->t('Units'),
      '#description' => $this->t('Select the units of the weather data.'),
      '#default_value' => $this->configuration['units'] ?? 'metric',
      '#options' => [
        'metric' => $this->t('Metric (Celsius, meter/second)'),
        'imperial' => $this->t('Imperial (Fahrenheit, miles/hour)'),
        'default' => $this->t('Standard (Kelvin, meter/second)'),
      ],
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['units'] = $form_state->getValue('units');
    $this->configuration['city'] = $form_state->getValue('city');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
//    dump($this->configuration);
    $build = $this->buildBaseInfo();

    return $build;
  }

  /**
   * Create render array with base weather information.
   *
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \PhpUnitConversion\Exception\UnsupportedConversionException
   */
  protected function buildBaseInfo() {
    $city = $this->configuration['city'] ?? 'London';
    $weather = $this->client->getCurrentWeatherByCity($city);
    $temperature = new Temperature\Kelvin($weather['main']['temp']);
    $converted_temperature = $temperature->to($this->getConversionClass($this->configuration['units']));

    $build = [];
    $build['weather']['#markup'] = '<h3>' . $weather['name'] . '</h3>' .
      '<p>' . $weather['weather'][0]['main'] . '</p>' .
      '<p>' . $converted_temperature . '</p>';

    return $build;
  }

  protected function getTemperatureMap() {
    return [
      'metric' => 'Celsius',
      'imperial' => 'Fahrenheit',
      'default' => 'Kelvin',
    ];
  }

  protected function getConversionClass($unit = NULL) {
    switch ($unit) {
      case 'default':
        return Temperature\Kelvin::class;
        break;
      case 'imperial':
        return Temperature\Fahrenheit::class;
        break;
      case 'metric':
      default:
        return Temperature\Celsius::class;
        break;
    }
  }
}

